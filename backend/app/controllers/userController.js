const mongoose = require('mongoose');
const shortid = require('shortid');
const time = require('./../libs/timeLib');
const response = require('./../libs/responseLib')
const logger = require('./../libs/loggerLib');
const logging = require('./../libs/logging');
const validateInput = require('../libs/paramsValidationLib')
const check = require('../libs/checkLib')
// const {isNullOrUndefined} = require('util');
// const tokenLib = require('../libs/tokenLib')


/* Models */
const UserModel = mongoose.model('User');

// create User function
let createUser = (req, res) => {

    let validatingInputs = () => {
        console.log("validatingInputs");
        return new Promise((resolve, reject) => {
            if ((req.body.role === 'Manager') ? (req.body.password && req.body.name && req.body.role) : (req.body.password && req.body.name && req.body.role && req.body.managerId)) {
                resolve(req);
            } else {
                let apiResponse = response.generate(true, "Parameters missing", 400, null);
                reject(apiResponse);
            }
        });
    }; // end of validatingInputs

    let checkManager = () => {
        console.log("checkManager");
        return new Promise((resolve, reject) => {
            if (req.body.role === 'Team Member') {
                UserModel.find({managerId: req.body.managerId}, function (err, managerDetails) {
                    if (err) {
                        logger.error("Failed to create user", "userController => checkManager()", 5);
                        let apiResponse = response.generate(true, err, 500, null);
                        reject(apiResponse);
                    } else if (check.isEmpty(managerDetails)) {
                        logger.error("Manager not found", "userController => checkManager()", 5);
                        let apiResponse = response.generate(true, "Manager not found", 500, null);
                        reject(apiResponse);
                    } else {
                        resolve();
                    }
                });
            } else {
                resolve();
            }
        });
    } // end of createUser function

    let createUsers = () => {
        console.log("createUser");
        return new Promise((resolve, reject) => {
            let body = {};
            if (req.body.role === 'Manager') {
                body['managerId'] = shortid.generate();
            }
            else {
                body['managerId'] = req.body.managerId;
                body['teamMemberId'] = shortid.generate();
            }
            body['name'] = req.body.name ? req.body.name : '';
            body['isActive'] = true;
            body['role'] = req.body.role;
            body['password'] = req.body.password;
            body['createdOn'] = new Date();
            UserModel.create(body, function (err, responsee) {
                if (err) {
                    logger.error("Failed to create user", "userController => createUsers()", 5);
                    let apiResponse = response.generate(true, err, 500, null);
                    reject(apiResponse);
                } else {
                    let finalObject = responsee.toObject();
                    delete finalObject._id;
                    delete finalObject.__v;
                    resolve(finalObject);
                }
            });
        });
    } // end of createUser function

    validatingInputs()
        .then(checkManager)
        .then(createUsers)
        .then((resolve) => {
            let apiResponse = response.generate(false, "User Created Successfully!!", 200, resolve);
            logging.captureError(200, "success", 'User Created Successfully!!', resolve)
                .then(()=>{
                    res.send(apiResponse);
                })
                .catch(()=>{
                    res.send(apiResponse);
                })
        })
        .catch((err) => {
            console.log(err);
            logging.captureError(500, "error", err, null)
                .then(()=>{
                    res.send(err);
                    res.status(err.status);
                })
                .catch(()=>{
                    res.send(err);
                    res.status(err.status);
                })
        });
}

module.exports = {
    createUser: createUser

}// end exports
