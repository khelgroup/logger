const bunyan = require('bunyan');
const mongoose = require('mongoose');
const response = require('./../libs/responseLib')
const shortid = require('shortid');
const logModel = mongoose.model('Log');

const captureError = function (status, resType, message, data) {
    return new Promise((resolve, reject) => {
        var log = bunyan.createLogger({name: 'myapp'});
        log = log.child({status: status, data: data});
        if (resType === 'success') {
            log.info(message)
        } else if (resType === 'error') {
            log.error(message)
        } else if (resType === 'warning') {
            log.warn(message)
        }
        let body = {};
        body['logId'] = shortid.generate();
        body['hostName'] = log.fields.hostname;
        body['message'] = message.message;
        body['level'] = log.streams[0].level;
        body['pid'] = log.fields.pid;
        body['status'] = status;
        body['createdOn'] = new Date();
        logModel.create(body, function (err, responsee) {
            if (err) {
                let apiResponse = response.generate(true, err, 500, null);
                reject(apiResponse);
            } else {
                let finalObject = responsee.toObject();
                delete finalObject._id;
                delete finalObject.__v;
                resolve(finalObject);
            }
        });
    });
}

module.exports = {
    captureError: captureError,
}// end exports