'use strict'
/**
 * Module Dependencies
 */
const mongoose = require('mongoose'),
    bcrypt = require('bcrypt'),
    Schema = mongoose.Schema,
    SALT_WORK_FACTOR = 10;

let logSchema = new Schema({
    logId: {
      type: String,
      default: '',
      // enables us to search the record faster
      index: true,
      unique: true
    },
    hostName: {
        type: String,
        default: ''
    },
    status: {
        type: String,
        default: ''
    },
    message: {
        type: String,
        default: ''
    },
    pid: {
        type: Number,
        default: null
    },
    level: {
        type: Number,
        default: null
    },
    createdOn :{
        type:Date,
        default:""
    }
}, { versionKey: false  })

mongoose.model('Log', logSchema);
