const express = require('express');
const router = express.Router();
const userController = require("./../../app/controllers/userController");
// const playerController = require("./../../app/controllers/playerController");
const appConfig = require("./../../config/appConfig")

const middleware = require('../middlewares/auth');
module.exports.setRouter = (app) => {

    let baseUrl = `${appConfig.apiVersion}/users`;

    // defining routes.


    // create user for system(password, firstName, role)
    app.post(`${baseUrl}/createUser`, userController.createUser);

}
